import { lazy } from "react";

const Sample = lazy(() => import("core/Public/Sample"));
const Checkout = lazy(() => import("core/Public/Checkout/Checkout"));
const Product = lazy(()=>import("core/Public/Product/product"))

export const appRoutes: CustomRoute[] = [
  {
    path: "/sample",
    component: Sample,
    type: "unauthorized",
  },
  {
    path: "/checkout",
    component: Checkout,
    type: "unauthorized",
  },
  {
    path: "/product",
    component: Product,
    type: "unauthorized",
  }
];
