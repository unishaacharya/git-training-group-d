import { AnyAction, combineReducers } from "redux";
import cartReducer from "./modules/cart/cart";

import i18nextReducer from "./modules/i18n/i18n";
import toggleCartReducer from "./modules/toggleCart/toggleCart";
import productDataReducer from "./modules/product";

export const appReducer = combineReducers({
  i18nextData: i18nextReducer,
  cart: cartReducer,
  toggleCart: toggleCartReducer,
  productData: productDataReducer,
});

export type RootState = ReturnType<typeof appReducer>;
type TState = ReturnType<typeof appReducer> | undefined;

export default function rootReducer(state: TState, action: AnyAction) {
  return appReducer(state, action);
}
