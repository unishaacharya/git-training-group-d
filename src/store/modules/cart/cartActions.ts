import { DefaultAction } from "store/actionNames";

import {
  ADD_TO_CART,
  DECREASE_QUANTITY,
  INCREASE_QUANTITY,
  REMOVE_FROM_CART,
} from "store/actionNames/cartActions/cartTypes";
import { CartState } from "./cart";

export const addToCart = (payload: CartState): DefaultAction => {
  return {
    type: ADD_TO_CART,
    payload: payload,
  };
};

export const removeFromCart = (payload: number): DefaultAction => {
  return {
    type: REMOVE_FROM_CART,
    payload: payload,
  };
};
export const increaseQuantity = (payload: number): DefaultAction => {
  return {
    type: INCREASE_QUANTITY,
    payload: payload,
  };
};

export const decreaseQuantity = (payload: number): DefaultAction => {
  return {
    type: DECREASE_QUANTITY,
    payload: payload,
  };
};
