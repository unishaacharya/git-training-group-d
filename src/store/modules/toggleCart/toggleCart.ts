import { DefaultAction } from "store/actionNames";
import {
  SHOW_CART,
  HIDE_CART,
} from "store/actionNames/toggleActions/toggleTypes";

type Viewcart = {
  viewCart: boolean;
};

const initialState: Viewcart = {
  viewCart: false,
};

const toggleCartReducer = (state = initialState, action: DefaultAction) => {
  switch (action.type) {
    case SHOW_CART:
      return {
        ...state,
        viewCart: true,
      };

    case HIDE_CART:
      return {
        ...state,
        viewCart: false,
      };

    default:
      return state;
  }
};

export default toggleCartReducer;
