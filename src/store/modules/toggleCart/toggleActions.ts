import { DefaultAction } from "store/actionNames";
import {
  SHOW_CART,
  HIDE_CART,
} from "store/actionNames/toggleActions/toggleTypes";

export const showCart = (): DefaultAction => {
  return {
    type: SHOW_CART,
  };
};

export const hideCart = (): DefaultAction => {
  return {
    type: HIDE_CART,
  };
};
