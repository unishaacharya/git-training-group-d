import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "store/root-reducer";
import { hideCart } from "store/modules/toggleCart/toggleActions";
import CartProductCard from "core/Public/productCart/ProductCart";
import { Link } from "react-router-dom";
import { getCommaSeperateNumber } from "utils/utilsFunction/number-converter";
import styled from "styled-components";
import { ImCross } from "react-icons/im";

const Cart = () => {
  const viewCart = useSelector((state: RootState) => state.toggleCart.viewCart);
  const cart = useSelector((state: RootState) => state.cart);
  const dispatch = useDispatch();

  const closeCart = () => {
    dispatch(hideCart());
  };

  const totalAmount = getCommaSeperateNumber(
    cart
      .map((item) => Number(item.price.substring(1)) * item.quantity)
      .reduce((acc, current) => {
        return acc + current;
      }, 0)
  );

  return (
    <>
      {viewCart && (
        <CartPage>
          <CartContainer>
            <CartHeader>
              <CloseButton onClick={closeCart} />
              Your Cart
            </CartHeader>
            <CartItems>
              {cart.map((product) => (
                <CartProductCard key={product.id} product={product} />
              ))}
            </CartItems>
            <CartFooter>
              <TotalPrice> Total Amount: &nbsp; Rs. {totalAmount}</TotalPrice>
              <Link to={"/checkout"}>
                <CheckoutButton disabled={cart.length === 0}>
                  Checkout
                </CheckoutButton>
              </Link>
            </CartFooter>
          </CartContainer>
        </CartPage>
      )}
    </>
  );
};

export default Cart;

export const CartPage = styled.section`
  height: 95vh;
  width: 100%;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 100%;
  z-index: 6;
  display: flex;
  justify-content: flex-end;
  margin-top: 50px;
`;

export const CartContainer = styled.div`
  height: 100%;
  background-color: white;
  width: min(500px, 100%);
  padding: 1rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;
export const CartHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-weight: 600;
`;

export const CloseButton = styled(ImCross)``;

export const CartItems = styled.ul`
  margin: 1rem auto;
  padding: 0;
  display: grid;
  grid-template-columns: 1;
  grid-gap: 1rem;
  overflow-y: auto;
  width: 100%;

  /* width */
  &::-webkit-scrollbar {
    width: 3px;
  }

  /* Track */
  &::-webkit-scrollbar-track {
    background: transparent;
  }

  /* Handle */
  &::-webkit-scrollbar-thumb {
    background: #009ef92f;
  }
`;

export const CartFooter = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-weight: 600;
`;

export const TotalPrice = styled.p``;

export const CheckoutButton = styled.button`
  border: none;
  height: 100%;
  width: max-content;
  background-color: #019bf5b7;
  padding: 5px 10px;
  display: flex;
  color: white;
  align-items: center;
  border-radius: 5px;
  margin-top: 10px;
  cursor: pointer;

  &:hover {
    transform: scale(1.1);
  }

  &:disabled {
    color: grey;
    background-color: #019cf583;

    &:hover {
      transform: none;
    }
  }
`;
