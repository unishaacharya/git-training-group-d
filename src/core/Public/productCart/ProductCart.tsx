import React from "react";
import { useDispatch } from "react-redux";
import { useLocation } from "react-router-dom";
import {
  decreaseQuantity,
  increaseQuantity,
  removeFromCart,
} from "store/modules/cart/cartActions";
import { getCommaSeperateNumber } from "utils/utilsFunction/number-converter";
import styled from "styled-components";
import { AiFillDelete, AiOutlinePlus, AiOutlineMinus } from "react-icons/ai";

type ProductData = {
  id: number;
  name: string;
  price: string;
  image: string;
  stock: number;
  createDate: string;
  quantity: number;
  category: string[];
};

const CartProductCard = ({ product }: { product: ProductData }) => {
  const dispatch = useDispatch();

  const deleteItem = (productId: number) => {
    dispatch(removeFromCart(productId));
  };

  /** Issues with render a step late than click action, Needs fixing later **/
  const increase = (productId: number, quantity: number, stock: number) => {
    if (quantity < stock) {
      dispatch(increaseQuantity(productId));
    }
  };
  const decrease = (productId: number, quantity: number) => {
    if (quantity >= 1) {
      if (quantity > 1) {
        dispatch(decreaseQuantity(productId));
      } else {
        dispatch(removeFromCart(productId));
      }
    }
  };

  const location = useLocation();

  return (
    <CartProductCardContainer>
      <DetailContainer>
        <CartProductImage
          src={`https://electronic-ecommerce.herokuapp.com/` + product.image}
        />
        <CartProductDetailContainer>
          <CartProductName>{product.name}</CartProductName>
          <CartProductPrice>
            Rs.
            {getCommaSeperateNumber(
              Number(product.price.substring(1)) * product.quantity
            )}
          </CartProductPrice>
          <CartProductQuantity>
            {location.pathname !== "/checkout" ? (
              <>
                <Plus
                  onClick={() =>
                    increase(product.id, product.quantity, product.stock)
                  }
                />
                {product.quantity}
                <Minus onClick={() => decrease(product.id, product.quantity)} />
              </>
            ) : (
              <>Qty:{product.quantity}</>
            )}
          </CartProductQuantity>
        </CartProductDetailContainer>
      </DetailContainer>
      {location.pathname !== "/checkout" && (
        <DeleteProduct onClick={() => deleteItem(product.id)} />
      )}
    </CartProductCardContainer>
  );
};

export default CartProductCard;

export const CartProductCardContainer = styled.li`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px;
  background-color: #009ef92f;
  border-radius: 5px;
  height: 100px;
  width: 100%;
  margin: 10px auto;
`;

export const DetailContainer = styled.div`
  display: flex;
  height: 80px;
`;
export const CartProductImage = styled.img`
  height: 100%;
  width: 80px;
  object-fit: fit;
`;
export const CartProductName = styled.p`
  font-weight: 600;
`;
export const CartProductPrice = styled.p``;
export const CartProductQuantity = styled.p`
  display: flex;
  align-items: center;
  font-weight: 600;
`;
export const DeleteProduct = styled(AiFillDelete)`
  height: 20px;
  width: 20px;
  color: red;
  transition: 300ms ease-in-out;
  cursor: pointer;

  &:hover {
    transform: scale(1.2);
  }
`;

export const Plus = styled(AiOutlinePlus)`
  height: 15px;
  width: 15px;
  transition: 300ms ease-in-out;
  cursor: pointer;
  margin-right: 10px;

  &:hover {
    transform: scale(1.2);
  }
`;

export const Minus = styled(AiOutlineMinus)`
  height: 15px;
  width: 15px;
  transition: 300ms ease-in-out;
  margin-left: 10px;
  cursor: pointer;

  &:hover {
    transform: scale(1.2);
  }
`;

export const CartProductDetailContainer = styled.div`
  margin-left: 10px;
`;
