import React, { useEffect, useState } from "react";
import "../Checkout/Checkout.scss";
import { getAllProduct } from "store/modules/product";
import { addToCart } from "store/modules/cart/cartActions";
import { showCart } from "store/modules/toggleCart/toggleActions";
import { connect, ConnectedProps, useDispatch, useSelector } from "react-redux";
import { RootState } from "store/root-reducer";
import { getCommaSeperateNumber } from "utils/utilsFunction/number-converter";
import "./product.scss";
import formatDate, {
  formatDateReverse,
} from "utils/utilsFunction/date-converter";
import Cart from "../productCart/Cart";
import moment from "moment";
const mapStateToProps = (state: RootState) => ({
  getProductData: state.productData,
});

const mapDispatchToProps = {
  getAllProduct,
};
export type ProductData = {
  id: number;
  name: string;
  price: string;
  image: string;
  stock: number;
  createDate: string;
  category: string[];
};

const connector = connect(mapStateToProps, mapDispatchToProps);

export type PlanListPropsFromRedux = ConnectedProps<typeof connector>;
interface IProps {
  PlanListPropsFromRedux;
  getProductData;
}

const Product: React.FC<IProps> = (props) => {
  const initialSearchingValues = {
    name: "",
    Maxprice: 0,
    Minprice: 0,
    category: "",
    MincreateDate: 0,
    MaxcreateDate: 0,
  };

  const [filterdata, setFilter] = useState([]);
  const [searching, setSearching] = useState(initialSearchingValues);
  const dispatch = useDispatch();
  const cart = useSelector((state: RootState) => state.cart);
  const viewCart = useSelector((state: RootState) => state.toggleCart.viewCart);
  useEffect(() => {
    const getItem = async () => {
      const response: any = await dispatch(getAllProduct());
      if (response.status) {
        let result: any = response.data?.product;
        setFilter(result);
      }
    };

    getItem();
  }, [dispatch]);

  const filterDAata = (sear: any) => {
    let result = props.getProductData.data?.product;
    if (sear.name) {
      result = result.filter((data) => {
        return data.name.toLowerCase().includes(sear.name);
      });
    }
    if (sear.category) {
      result = result.filter((data) => {
        return data.category.includes(sear.category);
      });
    }
    if (sear.Maxprice) {
      result = result.filter((data) => {
        return Number(data.price.split("$")[1]) <= sear.Maxprice;
      });
    }
    if (sear.Minprice) {
      result = result.filter((data) => {
        return Number(data.price.split("$")[1]) >= sear.Minprice;
      });
    }
    if (sear.MincreateDate) {
      result = result.filter((data) => {
        return (
          parseInt(data.createDate) >=
          Number(moment(sear.MincreateDate).startOf("day"))
        );
      });
    }

    if (sear.MaxcreateDate) {
      result = result.filter((data) => {
        return (
          parseInt(data.createDate) <=
          Number(moment(sear.MaxcreateDate).endOf("day"))
        );
      });
    }
    setSearching(sear);
    setFilter(result);
  };
  const datefilter = (event: any) => {
    const value = event.target.value;
    console.log(value, "show value");

    const name = event.target.name;
    let sear = { ...searching };
    sear[name] = value;
    filterDAata(sear);
  };
  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let name = event.target.name;
    let sear = { ...searching };
    sear[name] = value;

    filterDAata(sear);
  };

  const addProduct = (product: any) => {
    if (cart.filter((object) => object.id === product.id).length === 0) {
      dispatch(addToCart({ ...product, quantity: 1 }));
    } else {
      dispatch(addToCart({ ...product }));
    }
    dispatch(showCart());
  };
  console.log(filterdata?.[0]);

  console.log(moment().endOf("day").toDate().getTime().toString());

  const today = new Date();
  const tomorrow = new Date(today);
  tomorrow.setDate(tomorrow.getDate() + 1);

  return (
    <div className="container my-3">
      <h2 className="">Our Products</h2>
      <h5 className="mt-4">Filter</h5>
      <div className="row mb-4 mt-1 ">
        <div className="col">
          <div className="row">
            <div className="col-4 col-md-2 ">
              <label>Select Category</label>
              <select
                className="form-control w-100   "
                aria-label="Default select example"
                name="category"
                onChange={(event) => handleSearch(event)}
              >
                <option value="">All Category</option>
                <option value="laptop">Laptop</option>
                <option value="mobile">Mobile</option>
                <option value="Keyboard">Keyboard</option>
                <option value="headseat">Headseat</option>
                <option value="watch">Watch</option>
              </select>
            </div>

            <div className="col-4 col-md-2 ">
              <label>Min Price</label>
              <input
                className="form-control"
                type="number"
                placeholder={"Min Price"}
                name={"Minprice"}
                onChange={(event) => handleSearch(event)}
                min={0}
              />
            </div>

            <div className="col-4 col-md-2">
              <label>Max Price</label>
              <input
                className="form-control"
                type="number"
                placeholder={"Max Price"}
                name={"Maxprice"}
                min={0}
                onChange={(event) => handleSearch(event)}
              />
            </div>
            <div className="col-4 col-md-2">
              <label>Min Date</label>
              <input
                className="form-control"
                name="MincreateDate"
                type="date"
                placeholder={"Min Date"}
                onChange={(event) => datefilter(event)}
                max={formatDate(today)}
              />
            </div>
            <div className="col-4 col-md-2">
              <label>Max Date</label>
              <input
                className="form-control"
                name="MaxcreateDate"
                type="date"
                placeholder={"Max Date"}
                onChange={(event) => datefilter(event)}
                max={formatDate(tomorrow)}
              />
            </div>
          </div>
        </div>
        <div className="col-4 col-md-3 ml-auto">
          <label>Search Product</label>
          <input
            className="form-control "
            type="text"
            placeholder={"Search by Product name"}
            name={"name"}
            onChange={(event) => handleSearch(event)}
          />
        </div>
      </div>
      <div className="grid">
        {filterdata &&
          filterdata.length > 0 &&
          filterdata.map((data: any, i) => {
            return (
              <div className="card shadow" key={i}>
                <img
                  style={{
                    width: "100%",
                    height: "200px",
                    objectFit: "cover",
                  }}
                  src={`https://electronic-ecommerce.herokuapp.com/${data.image}`}
                  alt="product"
                  className="card-img-top"
                />
                <div className="card-body">
                  <h5 className="card-title mb-0 text-capitalize">
                    {data.name}
                  </h5>
                  <p className="text-capitalize ">{data.category[1]}</p>
                  <div className="mt-2">
                    <p className="">
                      Released:&nbsp;{formatDateReverse(data.createDate)}
                    </p>
                    <p className="">
                      Stock: &nbsp;
                      {data.stock === 0 ? (
                        <p style={{ fontSize: "12px", color: "red" }}>
                          Out of Stock
                        </p>
                      ) : (
                        data.stock
                      )}
                    </p>
                    <h6 className="my-2">
                      Rs. {getCommaSeperateNumber(data.price.split("$")[1])}
                    </h6>
                  </div>

                  <button
                    className="btn btn-outline-primary mt-2"
                    onClick={() => addProduct(data)}
                    disabled={data.stock === 0}
                  >
                    Add to Cart
                  </button>
                </div>
                {viewCart && <Cart />}
              </div>
            );
          })}
      </div>

      <div className="grid">
        {filterdata.length === 0 && <h2>No Data found </h2>}
      </div>
    </div>
  );
};

export default connector(Product);
