import React, { ReactElement, useState } from "react";
import FormGroup from "components/React/Form/FormGroup";
import FormTitle from "components/React/Form/FormTitle";
import * as Yup from "yup";
import { Formik, Form } from "formik";
import "./Checkout.scss";
import { SuccessToast } from "components/React/ToastNotifier/ToastNotifier";
import { formatDateReverse } from "utils/utilsFunction/date-converter";
import { getCommaSeperateNumber } from "utils/utilsFunction/number-converter";
import { useSelector } from "react-redux";
import { RootState } from "store/root-reducer";
import CartProductCard from "../productCart/ProductCart";

type InitialCheckoutValues = {
  billingFirstName: string;
  billingLastName: string;
  billingMiddleName?: string;
  billingAddress1: string;
  billingAddress2?: string;
  billingEmail: string;
  billingPhoneNo: string;
  billingDate: string;
  shippingFirstName: string;
  shippingLastName: string;
  shippingMiddleName?: string;
  shippingAddress1: string;
  shippingAddress2?: string;
  shippingPhoneNo: string;
  shippingEmail: string;
};

function Checkout(): ReactElement {
  const [formValues, setFormValues] =
    useState<InitialCheckoutValues | undefined>(undefined);
  const cart = useSelector((state: RootState) => state.cart);
  const initialValues: InitialCheckoutValues = {
    billingFirstName: "",
    billingLastName: "",
    billingMiddleName: "",
    billingAddress1: "",
    billingAddress2: "",
    billingEmail: "",
    billingPhoneNo: "",
    billingDate: "",
    shippingFirstName: "",
    shippingLastName: "",
    shippingMiddleName: "",
    shippingAddress1: "",
    shippingAddress2: "",
    shippingPhoneNo: "",
    shippingEmail: "",
  };

  const phoneRegExp =
    /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
  const today = new Date();
  today.setHours(0, 0, 0, 0);

  const validationSchema = Yup.object({
    billingFirstName: Yup.string().required("First Name Required"),
    billingLastName: Yup.string().required("Last Name Required"),
    billingAddress1: Yup.string().required("Enter Your Billing Address"),
    billingEmail: Yup.string()
      .email("Must be a valid Email")
      .required("Billing Email Required"),
    billingPhoneNo: Yup.string()
      .required("Billing Phone Number Required")
      .matches(phoneRegExp, "Phone number is not valid")
      .min(10, "to short")
      .max(10, "to long"),
    billingDate: Yup.date().nullable().required("Billing Date Required"),

    shippingFirstName: Yup.string().required("First Name Required"),
    shippingLastName: Yup.string().required("Last Name Required"),
    shippingAddress1: Yup.string().required("Enter Your Shipping Address"),
    shippingPhoneNo: Yup.string()
      .required("Shipping Phone Number Required")
      .matches(phoneRegExp, "Phone number is not valid")
      .min(10, "to short")
      .max(10, "to long"),
    shippingEmail: Yup.string()
      .email("Must be a valid Email")
      .required("Shipping Email Required"),
  });

  const onSubmit = (values: any) => {
    SuccessToast("Form Submitted Succesfully");
    setFormValues(values);
  };

  const totalAmount = getCommaSeperateNumber(
    cart
      .map((item) => Number(item.price.substring(1)) * item.quantity)
      .reduce((acc, current) => {
        return acc + current;
      }, 0)
  );

  return (
    <div className="container">
      <h1>Checkout</h1>
      <div className="row">
        <div className="col-12 col-md-8">
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
          >
            {({
              values,
              touched,
              errors,
              handleBlur,
              handleChange,
              handleSubmit,
              setFieldValue,
            }) => (
              <Form>
                <FormTitle formTitle="Billing Details" />
                <div className="checkout__grid">
                  <FormGroup
                    type="text"
                    name="billingFirstName"
                    value={values.billingFirstName}
                    commaSeparated={false}
                    errors={errors}
                    touched={touched}
                    formikHandleBlur={handleBlur}
                    formikHandleChange={handleChange}
                    placeholder="Billing First Name"
                    label="First Name"
                  />
                  <FormGroup
                    type="text"
                    name="billingMiddleName"
                    value={values.billingMiddleName}
                    commaSeparated={false}
                    errors={errors}
                    touched={touched}
                    formikHandleBlur={handleBlur}
                    formikHandleChange={handleChange}
                    placeholder="Billing Middle Name"
                    label="Middle Name"
                    notRequired
                  />

                  <FormGroup
                    type="text"
                    name="billingLastName"
                    value={values.billingLastName}
                    commaSeparated={false}
                    errors={errors}
                    touched={touched}
                    formikHandleBlur={handleBlur}
                    formikHandleChange={handleChange}
                    placeholder="Billing Last Name"
                    label="Last Name"
                  />
                </div>
                <div className="checkout__grid">
                  <FormGroup
                    type="text"
                    name="billingAddress1"
                    value={values.billingAddress1}
                    commaSeparated={false}
                    errors={errors}
                    touched={touched}
                    formikHandleBlur={handleBlur}
                    formikHandleChange={handleChange}
                    placeholder="Primary Billing Address "
                    label="Primary Address"
                  />
                  <FormGroup
                    type="text"
                    name="billingAddress2"
                    value={values.billingAddress2}
                    commaSeparated={false}
                    errors={errors}
                    touched={touched}
                    formikHandleBlur={handleBlur}
                    formikHandleChange={handleChange}
                    placeholder="Secondary Billing Address "
                    label="Secondary Address"
                    notRequired
                  />
                </div>
                <div className="checkout__grid">
                  <FormGroup
                    type="email"
                    name="billingEmail"
                    value={values.billingEmail}
                    commaSeparated={false}
                    errors={errors}
                    touched={touched}
                    formikHandleBlur={handleBlur}
                    formikHandleChange={handleChange}
                    placeholder="Billing Email Address"
                    label="Email Address"
                  />
                  <FormGroup
                    type="text"
                    name="billingPhoneNo"
                    value={values.billingPhoneNo}
                    commaSeparated={false}
                    errors={errors}
                    touched={touched}
                    formikHandleBlur={handleBlur}
                    formikHandleChange={handleChange}
                    placeholder="Billing Address Phone Number"
                    label="Phone Number"
                  />
                  <FormGroup
                    type="DatePicker"
                    name="billingDate"
                    commaSeparated={false}
                    errors={errors}
                    setFieldValue={setFieldValue}
                    touched={touched}
                    formikHandleBlur={handleBlur}
                    dateSettings={{
                      value: values.billingDate,
                      showYearDropdown: true,
                      maxDate: new Date(),
                    }}
                    label="Billing Date"
                  />
                </div>
                <FormTitle formTitle="Shipping Details" />
                <div className="checkout__grid">
                  <FormGroup
                    type="text"
                    name="shippingFirstName"
                    value={values.shippingFirstName}
                    commaSeparated={false}
                    errors={errors}
                    touched={touched}
                    formikHandleBlur={handleBlur}
                    formikHandleChange={handleChange}
                    placeholder="First Name of receiving person"
                    label="First Name"
                  />
                  <FormGroup
                    type="text"
                    name="shippingMiddleName"
                    value={values.shippingMiddleName}
                    commaSeparated={false}
                    errors={errors}
                    touched={touched}
                    formikHandleBlur={handleBlur}
                    formikHandleChange={handleChange}
                    placeholder="Middle Name of receiving person"
                    label="Middle Name"
                    notRequired
                  />
                  <FormGroup
                    type="text"
                    name="shippingLastName"
                    value={values.shippingLastName}
                    commaSeparated={false}
                    errors={errors}
                    touched={touched}
                    formikHandleBlur={handleBlur}
                    formikHandleChange={handleChange}
                    placeholder="Last Name of receiving person"
                    label="Last Name"
                  />
                </div>
                <div className="checkout__grid">
                  <FormGroup
                    type="text"
                    name="shippingAddress1"
                    value={values.shippingAddress1}
                    commaSeparated={false}
                    errors={errors}
                    touched={touched}
                    formikHandleBlur={handleBlur}
                    formikHandleChange={handleChange}
                    placeholder="Primary Shipping Address"
                    label="Primary Address"
                  />
                  <FormGroup
                    type="text"
                    name="shippingAddress2"
                    value={values.shippingAddress2}
                    commaSeparated={false}
                    errors={errors}
                    touched={touched}
                    formikHandleBlur={handleBlur}
                    formikHandleChange={handleChange}
                    placeholder="Secondary Shipping Address"
                    label="Secondary Address"
                    notRequired
                  />
                </div>
                <div className="checkout__grid">
                  <FormGroup
                    type="text"
                    name="shippingPhoneNo"
                    value={values.shippingPhoneNo}
                    commaSeparated={false}
                    errors={errors}
                    touched={touched}
                    formikHandleBlur={handleBlur}
                    formikHandleChange={handleChange}
                    placeholder="Shipping Address Phone Number"
                    label="Phone Number"
                  />
                  <FormGroup
                    type="email"
                    name="shippingEmail"
                    value={values.shippingEmail}
                    commaSeparated={false}
                    errors={errors}
                    touched={touched}
                    formikHandleBlur={handleBlur}
                    formikHandleChange={handleChange}
                    placeholder="Shipping Email Address"
                    label="Email Address"
                  />
                </div>
                <button
                  type="submit"
                  className="btn btn-outline bg-blue-20 text-white"
                >
                  Checkout Now
                </button>
              </Form>
            )}
          </Formik>
        </div>
        <div className="col-12 col-md-4">
          Your Items
          <div className="mt-5">
            {cart.map((product) => (
              <CartProductCard key={product.id} product={product} />
            ))}
          </div>
          <div className="mt-5">Total Amount: &nbsp; Rs. {totalAmount}</div>
        </div>
      </div>
      <div>
        {formValues ? (
          <div className="checkout__submitValues">
            <FormTitle formTitle="Submitted Details" />
            <div className="checkout__grid">
              <div>
                Billing Details
                <p>
                  Full Name:&nbsp;
                  {formValues.billingFirstName} &nbsp;
                  {formValues.billingMiddleName}&nbsp;
                  {formValues.billingLastName}
                </p>
                <p>Primary Address:&nbsp; {formValues.billingAddress1}</p>
                {formValues.billingAddress2 && (
                  <p>Secondary Address: &nbsp;{formValues.billingAddress2}</p>
                )}
                <p>
                  Phone No:&nbsp;
                  {formValues.billingPhoneNo}
                </p>
                <p>
                  Email:&nbsp;
                  {formValues.billingEmail}
                </p>
                <p>
                  Billed Date:&nbsp;
                  {formatDateReverse(formValues.billingDate)}
                </p>
              </div>
              <div>
                Shipping Details
                <p>
                  Full Name:&nbsp;
                  {formValues.shippingFirstName}&nbsp;
                  {formValues.shippingMiddleName}&nbsp;
                  {formValues.shippingLastName}
                </p>
                <p>Primary Address:&nbsp; {formValues.shippingAddress1}</p>
                {formValues.shippingAddress2 && (
                  <p>Secondary Address: &nbsp;{formValues.shippingAddress2}</p>
                )}
                <p>Phone No: &nbsp;{formValues.shippingPhoneNo}</p>
                <p>Email:&nbsp; {formValues.shippingEmail}</p>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    </div>
  );
}

export default Checkout;
